package task6;

public class Ryadu {
    public String solution(Double x, Integer n) {
        Double sum = 0.0;
        Double sumE = 0.0;
        Double sumE10 = 0.0;
        Double znachenieFunc = Math.pow(Math.E, x);
        for (int i = 0; i <= n; i++) {
            Double result = 1 + Math.pow(-1, n) * (Math.pow(x, 2 * n) / calculateFactorial(n));
            sum = sum + result;
            if (Math.abs(1 + Math.pow(-1, n) * (Math.pow(x, 2 * n) / calculateFactorial(n))) > Math.E) {
                sumE = sumE + 1 + Math.pow(-1, n) * (Math.pow(x, 2 * n) / calculateFactorial(n));
            }
            if (Math.abs(1 + Math.pow(-1, n) * (Math.pow(x, 2 * n) / calculateFactorial(n))) > Math.E / 10) {
                sumE10 = sumE10 + 1 + Math.pow(-1, n) * (Math.pow(x, 2 * n) / calculateFactorial(n));
            }
        }
        return ("Сумма n слагаемых заданного вида: " + sum + "\n" +
                "Сумма тех слагаемых, которые по абсолютной величине больше e: " + sumE + "\n" +
                "Сумма тех слагаемых, которые по абсолютной величине больше e/10: " + sumE10 + "\n" +
                "Значение функции с помощью методов Math: " + znachenieFunc);
    }

    static Integer calculateFactorial(Integer n) {
        int result = 1;
        for (int i = 1; i <= n; i++) {
            result = result * i;
        }
        return result;
    }
}

