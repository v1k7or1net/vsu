package task4;

public class ChislaLeonardo {
    private final static Double fi = ((1 + Math.sqrt(5)) / 2);

    public int chisloLeonardo(Integer n) {
        if (n == 0 || n == 1) {
            return 1;
        }
        Double chislo = (2 / Math.sqrt(5)) * (Math.pow(fi, n + 1) - Math.pow((1 - fi), n + 1) - 1);
        return chislo.intValue();
    }
    public Integer countChislaLeonardo(Integer n, Integer a, Integer b) {
        int count = 0;
        for (int i = 0; i <= n; i++) {
          int chisloLeonardo = chisloLeonardo(i);
            if (a <= chisloLeonardo && chisloLeonardo <= b) {
                count = count + 1;
            }
            System.out.println(chisloLeonardo);
        }
        return count;
    }
}