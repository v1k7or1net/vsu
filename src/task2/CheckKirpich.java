package task2;

public class CheckKirpich {
    public boolean kirpich(double a, double b, double c, double h, double w) {
        return (a <= w && b <= h) || (b <= w && a <= h) ||
                (a <= w && c <= h) || (c <= w && a <= h) ||
                (c <= w && b <= h) || (b <= w && c <= h);

    }
}

