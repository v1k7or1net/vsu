package task2;

import java.util.Scanner;

public class Task2App {
    public static void main(String[] args) {
        double a;
        double b;
        double c;
        double w;
        double h;
        Scanner scanner = new Scanner(System.in);
        CheckKirpich kirpichh = new CheckKirpich();
        System.out.println("Введите параметры кирпича");
        System.out.print("Длина кирпича");
        a = scanner.nextDouble();
        System.out.print("Высота кирпича");
        b = scanner.nextDouble();
        System.out.print("Ширина кирпича");
        c = scanner.nextDouble();
        System.out.println("Введите параметры отверстия");
        System.out.print("Ширина отверстия");
        w = scanner.nextDouble();
        System.out.print("Высота отверстия");
        h = scanner.nextDouble();
        if (kirpichh.kirpich(a, b, c, w, h))
            System.out.println("Проходит");
        else
            System.out.println("Не проходит");
    }
}
