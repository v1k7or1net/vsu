package task1;

import java.util.Scanner;

public class Task1App {
    public static void main(String[] args) {
        SquareArea squareArea = new SquareArea();
        Scanner scanner = new Scanner(System.in);
        double r1 = scanner.nextDouble();
        double R2 = scanner.nextDouble();
        System.out.println("Площадь закрашенной фигуры = " + squareArea.squareOfArea(r1, R2));
    }
}
