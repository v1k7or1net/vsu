package task1;

public class SquareArea {
    public double squareOfArea(double r1, double R2) {
        double squareOfSmallCircle = Math.PI * Math.pow(r1, 2);
        double squareOfBigCircleArea = (Math.PI * Math.pow(R2, 2) - squareOfSmallCircle) * 0.75;
        double squareOfFoursquareArea = Math.pow(R2, 2) - squareOfBigCircleArea / 3;
        return squareOfFoursquareArea + squareOfBigCircleArea;

    }

}
