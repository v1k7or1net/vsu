package task7;

public class Task7App {
    public static void main(String[] args) {
        Massivi massivi = new Massivi();

        //тест 1
        System.out.println(massivi.solution(new int[]{1, 2, 3, 4, 5, 7, 6, 5, 4, 0, 0, 0, 0}));

        //тест 2
        System.out.println(massivi.solution(new int[]{1, 1, 1, 1}));

        //тест 3
        System.out.println(massivi.solution(new int[]{0, 0, 0, 0}));

        //тест 4
        System.out.println(massivi.solution(new int[]{1, 2}));

        //тест 5
        System.out.println(massivi.solution(new int[]{5}));

        //тест 6
        System.out.println(massivi.solution(new int[]{5, 6, 0, 8, 9, 10, 11, 12, 15}));

        //тест 7
        System.out.println(massivi.solution(massivi.vvodMassiva(2)));

        //тест 8
        System.out.println(massivi.solution(massivi.vvodMassiva(1)));

        //тест 9
        System.out.println(massivi.solution(massivi.vvodMassiva(0)));
    }
}
