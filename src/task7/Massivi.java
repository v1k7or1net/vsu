package task7;

import java.util.Scanner;

public class Massivi {
    public int[] vvodMassiva(int n) {
        int[] massiv = new int[n];
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < n; i++) {
            massiv[i] = scanner.nextInt();
        }
        return massiv;
    }

    public void vivodMassiva(int[] massiv) {
        for (int i = 0; i < massiv.length; i++) {
            System.out.print(massiv[i] + " ");
        }
    }
    public int solution(int[] massiv) {
        if (massiv.length == 0) {
            return 0;
        }
        if (massiv.length == 1) {
            return massiv[0];
        }
        if (massiv.length == 2) {
            return massiv[0] + massiv[1];
        }
        int sum = 0;
        int maxSum = 0;
        for (int i = 1; i < massiv.length - 1; i++) {
            if (massiv[i] - massiv[i - 1] == massiv[i + 1] - massiv[i]) {
                sum = massiv[i-1] + sum;
            }
            else if (sum+massiv[i]+massiv[i-1] > maxSum) {
                maxSum = sum + massiv[i]+massiv[i-1];
                sum = 0;
            }
        }
        return maxSum;
    }
}
