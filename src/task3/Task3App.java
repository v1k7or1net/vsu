package task3;

import java.util.Scanner;

public class Task3App {
    public static final ParabolaRight P1 =
            new ParabolaRight(-6, -6, 0.5);
    public static final ParabolaLeft P2 =
            new ParabolaLeft(-3, -4, -0.25);
    public static final Circle C1 =
            new Circle(5, 0);

    public static SimpleColor getColor(double x, double y) {
        if (P1.isPointRightOfParabola(x, y) && C1.isPointInCircle(x, y) || !P1.isPointRightOfParabola(x, y) && P2.isPointLeftOfParabola(x, y)) {
            return SimpleColor.BLUE;
        }
        if (C1.isPointInCircle(x, y) && !P1.isPointRightOfParabola(x, y)) {
            return SimpleColor.GRAY;
        }
        if (P1.isPointRightOfParabola(x, y) && !C1.isPointInCircle(x, y) && !P2.isPointLeftOfParabola(x, y)) {
            return SimpleColor.GREEN;
        }
        if (P2.isPointLeftOfParabola(x, y) && P1.isPointRightOfParabola(x, y)) {
            return SimpleColor.ORANGE;
        }
        return SimpleColor.WHITE;
    }

    public static void printColorForPoint(double x, double y) {
        System.out.println("(" + x + ", " + y + ")" + " -> " + getColor(x, y));
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double x = scanner.nextDouble();
        double y = scanner.nextDouble();
        printColorForPoint(x, y);
    }
}

