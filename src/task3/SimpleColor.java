package task3;

public enum SimpleColor {
    RED,
    BLUE,
    GREEN,
    ORANGE,
    WHITE,
    GRAY;
}
