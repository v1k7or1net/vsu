package task3;

public class Circle {
    public double x0;
    public double y0;

    public Circle(double x0, double y0) {
        this.x0 = x0;
        this.y0 = y0;
    }

    public boolean isPointInCircle(double x, double y) {
        return 25 > Math.pow((x0 - x), 2) + Math.pow((y0 - y), 2);
    }
}