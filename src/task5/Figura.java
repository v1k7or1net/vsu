package task5;

public class Figura {
    public void pechatFiguri(int h, int w) {
        int a = 0;
        for (int i = 0; i < h; i++) {
            for (int j = 0; j < w; j++) {
                if (j < i) {
                    System.out.print(" ");
                } else {
                    if (j == i) {
                        a = 0;
                    }
                    System.out.print(a);
                    a++;
                    if (a > 9) {
                        a = 0;
                    }
                }
            }
            System.out.println();
        }
    }
}
